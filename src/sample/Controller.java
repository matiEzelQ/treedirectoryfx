package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.event.EventType;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import java.io.File;
import java.util.List;


import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Node;

public class Controller {

    public Controller(Stage stage) {
        stage.setScene(new Scene(parent));

        File file = new File("/Users/MatiEzelQ/Documents/1ra carpeta A");
        TreeItem<String> treeItem = new TreeItem<>(file.getName());
        TreeView<String> treeView = new TreeView<String>(searchFiles(file,treeItem));


    }

    private TreeItem<String> searchFiles(File file,TreeItem<String> parentItem) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (int i=0;i<files.length;i++) {
                if (!files[i].isHidden()) {
                    if (files[i].isDirectory()) {//Si file 1 es un directorio...
                        TreeItem<String> child = new TreeItem<String>(files[i].getName());//Creo un TreeItem del directorio...
                        parentItem.getChildren().addAll(child);//Se lo agrego al parentItem...
                        searchFiles(new File(files[i].getAbsolutePath()), child);//y hago lo mismo con el directorio.
                    } else {
                        parentItem.getChildren().add(new TreeItem<String>(files[i].getName()));//Creo un TreeItem del archivo y lo agrego al padre.
                    }
                }
            }
        } else {
            parentItem.getChildren().add(new TreeItem<>(file.getName()));
        }
        return parentItem;
    }




}
